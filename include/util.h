#ifndef UTIL_H
#define UTIL_H
#include "Config.h"
#ifdef USE_Eigen
#include <Eigen/Dense>
#endif

namespace Util {
/**
 * Useless class that can add values to a matrix
 * @author Reed Watson
 */
struct Stuff {
    int a   = 0; ///< addition
    float b = 0.f; ///< multiplication
    /**
     * Combine Stuff with other stuff
     * a' = a1 + a2; b' = b1 + b2;
     * @param  other another Stuff.
     * @return a new Stuff object
     */
    Stuff combine(const Stuff & other) const;
#ifdef USE_Eigen
    Eigen::Matrix2f apply(const Eigen::Matrix2f &) const;
#endif
};

} // namespace Util

#endif
