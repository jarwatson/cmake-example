# cmake-example

Basic example of a cmake project with the following features:
* Shared Library
* Executable
* Installation
* Exported Targets
* External build importing the library.
* Configuration file
* Third-party library linkage.

## Usage

```bash
git clone git@gitlab.com:jarwatson/cmake-example.git
cd cmake-example
cmake .
make
make install
./install/bin/Tutorial
```
