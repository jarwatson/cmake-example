#include "util.h"
#include "Config.h"
#include <cmath>
#include <cassert>
#include <iostream>
using Util::Stuff;

int main(int argc, char** argv){
    Stuff mystuff = {1,2};
    Stuff otherstuff = {3,4};
    auto result = mystuff.combine(otherstuff);
    assert(result.a == 4);
    assert(fabs(result.b - 8) < 1e-14);
    std::cout << "Success " << Config::MajorVersion  << "." << Config::MinorVersion <<" !\n";
    return 0;
};
