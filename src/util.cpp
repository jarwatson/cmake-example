#include "util.h"

using Util::Stuff;

#ifdef USE_Eigen
using Eigen::Matrix2f;
Matrix2f Stuff::apply(const Matrix2f& other) const{
    return  b * (other + a*Matrix2f::Identity());
}
#endif
Stuff Stuff::combine(const Stuff& other) const{
    Stuff newstuff = other;
    newstuff.a += a;
    newstuff.b *= b;
    return newstuff;
}
