#include "util.h"
#include "Config.h"
#include <cmath>
#include <cassert>
#include <iostream>
using Util::Stuff;

int main(int argc, char** argv){
    std::cout << "Calling Stuff::combine()\n";
    Stuff mystuff = {1,2};
    Stuff otherstuff = {3,4};
    auto result = mystuff.combine(otherstuff);
    assert(result.a == 4);
    assert(fabs(result.b - 8) < 1e-14);
#ifdef USE_Eigen
    std::cout << "Calling Stuff::apply()\n";
    Eigen::Matrix2f m;
    m << 1, 2, 3, 4;
    auto mresult = mystuff.apply(m);
    assert(fabs(mresult(0,0) - 4) < 1e-14);
    assert(fabs(mresult(0,1) - 4) < 1e-14);
    assert(fabs(mresult(1,0) - 6) < 1e-14);
    assert(fabs(mresult(1,1) - 10) < 1e-14);
#endif
    std::cout << "Success " << Config::MajorVersion  << "." << Config::MinorVersion <<" !\n";

    return 0;
};
